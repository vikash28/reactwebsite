var cacheFlag = false; // make it false to true for production
 
var NAME_OF_CACHE = "SipnoTech-dev-cache";
 
self.addEventListener("activate", event => {
  const cacheWhitelist = [NAME_OF_CACHE];
  event.waitUntil(
    caches.keys().then(keyList =>
      //console.log(keyList);
      Promise.all(
        keyList.map(key => {
          console.log(key);
          caches.delete(key)
          // if(key.includes("deskhelp"))
          // {
          //   caches.delete(key)
          // }
        })
      //   keyList .filter(function(key) {
      //     caches.delete(key);// Return true if you want to remove this cache,
      //     // but remember that caches are shared across
      //     // the whole origin
      // }).map(function(key) {
      //     return caches.delete(key);
      // })
       )
    )
  );
});
 
self.addEventListener("install", function(event) {
  if (cacheFlag) {
    event.waitUntil(
      caches.open(NAME_OF_CACHE).then(function(cache) {
        fetch("manifest.json")
          .then(response => {
            response.json();
          })
          .then(assets => {
            const addurlsToCache = ["/", assets["main.js"]];
            cache.addAll(addurlsToCache);
            console.log("is it stored in cache");
          });
      })
    );
  }
});
 
self.addEventListener("fetch", function(event) {
  if (cacheFlag) {
    event.respondWith(
      caches.match(event.request).then(function(response) {
        console.log("fetch event");
        return response || fetch(event.request);
      })
    );
  }
});